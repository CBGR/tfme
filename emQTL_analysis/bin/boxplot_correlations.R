require(rowr)
args = commandArgs(trailingOnly=TRUE);
master_corr = args[length(args) - 2]
outname = args[length(args)]
nbprobes = as.numeric(args[length(args) - 1])
files = args[1:(length(args) - 3)]

# colMedians from https://github.com/cran/miscTools/blob/master/R/colMedians.R
colMedians <- function( x, na.rm = FALSE ) {

       if( is.data.frame( x ) ) {
                 x <- as.matrix( x )
   }
   if( !is.array( x ) ) {
             stop( "argument 'x' must be a data.frame, matrix, or array" )
      }
      if( !is.numeric( x ) ) {
                stop( "argument 'x' must be numeric" )
         }

      result <- array( NA, dim = dim( x )[-1] )
         dimnames( result ) <- dimnames( x )[-1]

         for( i in 1:dim( x )[ 2 ] ) {
                   if( length( dim( x ) ) == 2 ) {
                                result[ i ] <- median( x[ , i ], na.rm = na.rm
                                                      )
               } else {
                            result[ slice.index( result, 1 ) == i ] <-
                                            colMedians( array( x[ slice.index(
                                                                              x,
                                                                              2
                                                                              )
               == i ],
                              dim = dim( x )[ -2 ] ), na.rm = na.rm )
                     }
            }

            return( result )
}

df = data.frame()
for(f in files){
    dn = gsub("-", ".", dirname(f));
    tmp = read.table(f, col.names=c('probes', dn))
    df = cbind.fill(df, tmp[tmp$probes>nbprobes,], fill=NA)
}
master = read.table(master_corr, col.names=c('probes', 'corr'))
quant = quantile(master[master$probes>nbprobes,]$corr)
limit = quant[4] + 1.5 * (quant[4] - quant[2])
df = df[,gsub("-", ".", lapply(files, dirname))]
#medians = colMedians(df, na.rm=T)
means = colMeans(df, na.rm=T)
svg(paste0(outname, '.svg'))
boxplot(df[,order(means)],
#boxplot(df[,order(medians)],
        ylab="Fraction of correlated CpGs", las=2)
abline(h=limit, col='blue')
null = dev.off()

# grabbing the plot scene as a grid object
library(gridGraphics)
emQTL.box.plot <- recordPlot()
plot.new()
emQTL.box.plot
grid.echo()
emQTL.box.plot <- grid.grab()

# Export plot as Rdata object
save(emQTL.box.plot, file = paste0(outname, ".rdata")




