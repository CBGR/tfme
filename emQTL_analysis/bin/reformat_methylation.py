# coding: utf-8

# In[1]:

import pandas as pd
import sys
import getopt


###############################################################################
#                               MAIN
###############################################################################
if __name__ == "__main__":
    usage = '''
    %s -m <methylation file> -s <specimen> -M <methylation out>
    ''' % (sys.argv[0])

    try:
        opts, args = getopt.getopt(sys.argv[1:], "m:M:s:h")
    except getopt.GetoptError:
        sys.exit(str(getopt.GetoptError) + usage)

    methylation_file = None
    meth_output = None
    specimen_file = None
    for o, a in opts:
        if o == '-m':
            methylation_file = a
        elif o == '-M':
            meth_output = a
        elif o == '-s':
            specimen_file = a
        else:
            sys.exit(usage)
    if not(methylation_file and meth_output and specimen_file):
        sys.exit(usage)

    specimens = pd.read_csv(specimen_file, sep='\t', header=0,
                            usecols=['icgc_specimen_id', 'specimen_type'])
    tumor_specimens = [row[1]['icgc_specimen_id']
                       for row in specimens.iterrows() if
                       not row[1]['specimen_type'].startswith('Normal')]

    import os
    methylation_dict = {}
    if os.stat(methylation_file).st_size:
        methylation = pd.read_csv(methylation_file, sep='\t',
                                  names=['donor', 'project', 'specimen',
                                         'icgc_sample', 'submitted_sample',
                                         'analysis', 'platform', 'probe',
                                         'meth_value', 'metric',
                                         'meth_intensity', 'unmeth_intensity',
                                         'verification_status',
                                         'verification_platform',
                                         'fraction_covered', 'conversion_rate',
                                         'protocol', 'other_analysis',
                                         'directory', 'accession'],
                                  usecols=['probe', 'donor', 'specimen',
                                           'meth_value'])

        for row in methylation.iterrows():
            if row[1]['specimen'] in tumor_specimens:
                cat = row[1]['donor']
                if not cat in methylation_dict:
                    methylation_dict[cat] = {}
                methylation_dict[cat][row[1]['probe']] = row[1][
                    'meth_value']
    df = pd.DataFrame.from_dict(methylation_dict)
    df.to_csv(meth_output, sep='\t', index=True)
