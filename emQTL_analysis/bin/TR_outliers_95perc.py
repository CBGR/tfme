#!/usr/bin/python
#*-* coding: utf-8 *-*

""" Extract the TFs of interest with enough correlated probes. """

import sys
import getopt


def extract_minimum_corr_probes(infile, min_probes):
    import pandas as pd
    corr_data = pd.read_csv(infile, sep=' ', names=['nb_probes', 'corr'])
    try:
        top95 = corr_data[corr_data['nb_probes'] > min_probes].quantile(
            .95)['corr']
        #print('qone: {0:f}, qthree: {1:f}'.format(qone, qthree))
        return top95
    except ValueError:
        return 1.


###############################################################################
#                               MAIN
###############################################################################
if __name__ == "__main__":
    usage = '''
    %s -t <TF list> -d <directory> -e <file extension> -p <project> \
            -c <correlations> -C <all cancer correlations>\
            [-m <min nb probes>] [-i <nb IQR away>] [-u <MWU test pval>]

        -m: min number of probes (default: 10000)
        -i: number of IQR away from Q3 to call outliers (default: 1.5)
        -u: Mann-Whitney U-test p-value threshold to be used (default: 1)
    ''' % (sys.argv[0])

    try:
        opts, args = getopt.getopt(sys.argv[1:], "t:d:c:m:e:p:u:C:h")
    except getopt.GetoptError:
        sys.exit(str(getopt.GetoptError) + usage)

    TF_list_file = None
    directory = None
    corr_file = None
    master_corr_file = None
    ext = None
    project = None
    min_prob = 10000
    iqr_away = 1.5
    mwu = 1.
    for o, a in opts:
        if o == '-t':
            TF_list_file = a
        elif o == '-d':
            directory = a
        elif o == '-c':
            corr_file = a
        elif o == '-C':
            master_corr_file = a
        elif o == '-m':
            min_prob = eval(a)
        elif o == '-e':
            ext = a
        elif o == '-p':
            project = a
        elif o == '-u':
            mwu = eval(a)
        else:
            sys.exit(usage)
    if not(TF_list_file and directory and corr_file and master_corr_file and
           ext and project):
        sys.exit(usage)

    #min_corr_probes = extract_minimum_corr_probes(corr_file, min_prob,
    #                                              iqr_away)
    # min_corr_probes = max(extract_minimum_corr_probes(corr_file, min_prob,
                                                      # iqr_away),
                          # extract_minimum_corr_probes(master_corr_file,
                                                      # min_prob, iqr_away))

    min_corr_probes = extract_minimum_corr_probes(master_corr_file, min_prob)
    #print('min_corr_probes: {0}'.format(min_corr_probes))
    print(
        'tf\tnb_cpg\tnb_corr\tpercent_corr\tnb_pos_corr\tpercent_pos_corr\tnb_neg_corr\tpercent_neg_corr\tmann_whitney_u_test')
    with open(TF_list_file) as stream:
        import pandas as pd
        import os.path
        for tf in stream:
            the_file = '{0}/{1}/{2}_{1}{3}.txt'.format(directory, tf.strip(),
                                                       project, ext)
            if os.path.isfile(the_file):
                try:
                    corr_res = pd.read_csv(the_file, sep='\t', header=0)
                    if(not(corr_res.empty) and corr_res['nb_cpg'][0] > min_prob
                       and corr_res['percent_corr'][0] > min_corr_probes):
                        if (mwu == 1. or
                              corr_res['mann_whitney_u_test'][0] <= mwu):
                            print ('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}'.format(
                                tf.strip(), corr_res['nb_cpg'][0],
                                corr_res['nb_corr'][0],
                                corr_res['percent_corr'][0],
                                corr_res['nb_pos_corr'][0],
                                corr_res['percent_pos_corr'][0],
                                corr_res['nb_neg_corr'][0],
                                corr_res['percent_neg_corr'][0]), end="")
                            if(mwu < 1.):
                                print ('\t{0}'.format(corr_res['mann_whitney_u_test'][0]))
                            else:
                                print ('')
                except pd.errors.EmptyDataError:
                    pass
