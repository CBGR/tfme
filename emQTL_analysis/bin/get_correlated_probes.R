args = commandArgs(trailingOnly=T)

rdata = args[1]
output_name = args[2]

probes = vector();
if(file.info(rdata)$size){
    load(rdata);
    if(exists("meth_eqtl")){
        probes = rownames(meth_eqtl);
    }
}
write(as.vector(probes), file=paste0(output_name, ".probes.txt"));
