# TFme pipeline
## **About**
This repo contains the workflow used to conduct emQTL computation between TF expression and DNAmethylation in cancer patients from TCGA, used in the [manuscript](https://www.biorxiv.org/content/10.1101/2021.05.10.443359v1).
It contains two subdirectories:

## A. **emQTL_analysis and**

 - the Snakefile containing the the work-flow is located under this sub directory
 - Python and R scripts, which are part of some rules in the TFme snakemake pipeline are located in the `bin` folder under this sub directory
 - programs that are required by the pipeline can be placed in the `src` folder under this sub directory

## B. **EPIC_meArray_analysis**

 - R code used to pre-process the raw IDAT files from the EPIC methylation array experiment, produced in this study along with the differential methylation (DMR) analysis is provided under this sub directory
 - the raw data in IDAT format can be downloaded from GEO *accession ID:GSE174008* and be placed in the `raw_data` folder under this sub directory
 - the FOXA1 binding regions in MCF-7 cells from ReMap database, which were used to carry out the DMR analysis are provided in `ReMap_MCF7_FOXA1_peaks` folder under this subdirectory

---

## **_Requirements to reproduce the emQTL results generated in the manuscript_**

### 1. **Software requirements**

- snakemake version 5.5.4
- R version 3.5.1
- Python 3.6.12
- [eMap](http://www.bios.unc.edu/~weisun/software.htm) R package version 1.2
- [bedtools](https://github.com/arq5x/bedtools2/releases) version 2.26.0

  *_to download this specific bedtools version type the following_*

 ```
 wget https://github.com/arq5x/bedtools2/releases/download/v2.26.0/bedtools-2.26.0.tar.gz
 tar xvfz bedtools-2.26.0.tar.gz
 ls -l bedtools2 # to see the different src files, genome files, README, and etc
 ```

- UCSC [liftOver](http://hgdownload.soe.ucsc.edu/admin/exe/) binary file

---

### 2. **Input data requirements**

- UniBind TFBSs from UniBind database (version 2018) are provided in `data/UniBind/`
- TCGA 450k methylation array data (meth.tsv file, which is a table of tab separated values) for the 19 cancer types considered in this study
- TCGA RNA-seq data (expr.tsv file, which is a table of tab separated values) for the 19 cancer types considered in this study
- TCGA specimen.tsv files corresponding to each of the 19 cancer types considered
- All TCGA related data for the specific cohorts can be downloaded from the [ICGC data portal](https://dcc.icgc.org/)
 
*_To show the structure of the TCGA input files, example data is provided in `emQTL_analysis/raw/` for_*

- meth.tsv (random subset from BLCA TCGA methylation data)
- expr.tsv (random subset from BLCA TCGA expression data) and
- specimen.tsv for BLCA-US

### 3. **Additional data requirements**

- HG38 human genome file (human.hg38.genome) can be copied from `bedtools2/genomes/` (from step 1) and placed at `TFme/emQTL_analysis/src`   *the Snakefile points to this path*
- hg38 to hg19 liftover chain ([hg38ToHg19.over.chain](http://hgdownload.soe.ucsc.edu/goldenPath/hg38/liftOver/)) can be downloaded from UCSC and be placed in the `TFme/emQTL_analysis/src` folder  *the Snakefile points to this path*
---


### **_Running the emQTL_analysis _**

```
cd emQTL_analysis
snakemake
```
---

### **_Running the EPIC_meArray_analysis_**
#### *Software requirement*
  - R version 4.0.2
	- [minfi](https://bioconductor.org/packages/release/bioc/html/minfi.html) Bioconductor R package version 1.36.0
	- [limma](https://bioconductor.org/packages/release/bioc/html/limma.html) Bioconductor R package version 3.46.0
	- [mcSEA](https://www.bioconductor.org/packages/release/bioc/html/mCSEA.html) Bioconductor R package version 1.10.0

```
cd EPIC_meArray_analysis
Rscript mCSEA_script.R
```
---
